import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DatatablePokedexComponent } from './components/datatable-pokedex/datatable-pokedex.component';
import { ModalPokemonComponent } from './component/modal-pokemon/modal-pokemon.component';

const routes: Routes = [{
  path: 'datatable',
  component: DatatablePokedexComponent
},
{
  path: 'modal',
  component: ModalPokemonComponent
}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
