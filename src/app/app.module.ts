import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DatatablePokedexComponent } from './components/datatable-pokedex/datatable-pokedex.component';
import {MatCardModule} from '@angular/material/card';
import {MatTableModule} from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material/paginator';
import { HttpClientModule } from '@angular/common/http';
import {MatDialogModule} from '@angular/material/dialog';
import { ModalPokemonComponent } from './component/modal-pokemon/modal-pokemon.component';
import { ModalPokemonDialogComponent } from './component/modal-pokemon-dialog/modal-pokemon-dialog.component';


@NgModule({
  declarations: [
    AppComponent,
    DatatablePokedexComponent,
    ModalPokemonComponent,
    ModalPokemonDialogComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatCardModule,
    MatTableModule,
    MatPaginatorModule,
    HttpClientModule,
    MatDialogModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
