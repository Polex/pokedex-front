export interface PokemonReponse {
    data:PokemonInfo[],
    next:string,
    previous:string,
    count:number
}


export interface PokemonInfo {
    id:string,
    name:string
}