import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalPokemonDialogComponent } from './modal-pokemon-dialog.component';

describe('ModalPokemonDialogComponent', () => {
  let component: ModalPokemonDialogComponent;
  let fixture: ComponentFixture<ModalPokemonDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModalPokemonDialogComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ModalPokemonDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
