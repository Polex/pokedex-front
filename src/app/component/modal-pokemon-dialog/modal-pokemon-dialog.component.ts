import { Component, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-modal-pokemon-dialog',
  templateUrl: './modal-pokemon-dialog.component.html',
  styleUrls: ['./modal-pokemon-dialog.component.scss']
})
export class ModalPokemonDialogComponent {
  constructor(public dialogRef: MatDialogRef<ModalPokemonDialogComponent> ) {}
}
