import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DatatablePokedexComponent } from './datatable-pokedex.component';

describe('DatatablePokedexComponent', () => {
  let component: DatatablePokedexComponent;
  let fixture: ComponentFixture<DatatablePokedexComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DatatablePokedexComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DatatablePokedexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
