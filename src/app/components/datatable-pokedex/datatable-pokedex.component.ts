import { AfterViewInit, Component, ViewChild } from '@angular/core';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import { merge, Observable, of as observableOf, pipe } from 'rxjs';
import { catchError, map, startWith, switchMap } from 'rxjs/operators';
import { PokemonInfo } from 'src/app/models/PokemonInfo.model';
import { PokemonSpeciesService } from 'src/app/services/datatablePokemon/pokemon-species.service';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { ModalPokemonDialogComponent } from 'src/app/component/modal-pokemon-dialog/modal-pokemon-dialog.component';
import { MatDialog } from '@angular/material/dialog';



@Component({
  selector: 'app-datatable-pokedex',
  templateUrl: './datatable-pokedex.component.html',
  styleUrls: ['./datatable-pokedex.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0', visibility: 'hidden' })),
      state('expanded', style({ height: '*', visibility: 'visible' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ]
})
export class DatatablePokedexComponent implements AfterViewInit {
  currentPage = 0;
  totalPages = 0;
  totalData = 0;
  pokemonData! : PokemonInfo[];
  displayedColumns: string[] = ['Id', 'Nombre'];
  dataSource = new MatTableDataSource<PokemonInfo>();

  isExpansionDetailRow = (i: number, row: Object) => row.hasOwnProperty('detailRow');
  expandedElement: any;

  clickRow(element:PokemonInfo){
    console.log(element.name);
  }

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  
  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.paginator.pageIndex = 0;
    this.paginator.pageSize = 20;
  ;

    this.paginator.page
    .pipe(
      startWith({}),
      switchMap(() => {
        return this.listar2(
          this.paginator.pageIndex * 20
        ).pipe(catchError(() => observableOf(null)));
      }),
      map((response) => {
        if (response == null) return [];
        this.totalData = response.count;
        return response.data;
      })
    )
    .subscribe((pokemonInfo) => {
      this.pokemonData = pokemonInfo;
      this.dataSource = new MatTableDataSource(this.pokemonData);
    });

  }


  constructor(private pokeService:PokemonSpeciesService, public dialog: MatDialog) {}

  listar2(offset:number){
    return this.pokeService.getPokemon(offset);
  }

  

  openDialog(enterAnimationDuration: string, exitAnimationDuration: string, element: PokemonInfo): void {

    this.dialog.open(ModalPokemonDialogComponent, {
      width: '250px',
      enterAnimationDuration,
      exitAnimationDuration,
    });
 
}





}
