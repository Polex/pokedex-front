import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { PokemonReponse } from 'src/app/models/PokemonInfo.model';

@Injectable({
  providedIn: 'root'
})
export class PokemonSpeciesService {
  url:string = "http://localhost:8080/pokemon-api/pokemon?offset="
  constructor(private _http: HttpClient) {  }

  getPokemon(offset:number):Observable<PokemonReponse>{
    return this._http.get<PokemonReponse>(this.url + offset);
  }
}
